"
I represent a Jira Sprint, a Jira concept that groups issues (which are a representation of  a type of UserStory in Agile nomenclature).

I lazily load issues as this can be a performance bottle neck - and some of my ""values"" (like  velocity) rely on issue data to compute their result.
"
Class {
	#name : #JiraSprint,
	#superclass : #JiraItem,
	#instVars : [
		'issues'
	],
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
JiraSprint class >> fromJira: aJira withId: anIdNumber [

	| result |
	result := aJira execute: (JiraAgileSprintResource new getSprint: anIdNumber).
	
	^ self withJira: aJira andData: result
]

{ #category : #accessing }
JiraSprint >> boardId [
	^ self dataValue: 'originBoardId'
]

{ #category : #values }
JiraSprint >> commitment [

	^self issues inject: 0 into: [ :total :issue | 
		issue storyPoints ifNil: [total] ifNotNil: [:v | total + v ] ]
]

{ #category : #accessing }
JiraSprint >> completedDate [
	^DateAndTime fromString: (self dataValue: 'completeDate' ifAbsent: [^nil])
]

{ #category : #values }
JiraSprint >> duration [
	^self isCompleted
		ifTrue: [
			self completedDate asDate - self startDate asDate ]
		ifFalse: [  0 ]
]

{ #category : #accessing }
JiraSprint >> endDate [
	^DateAndTime fromString: (self dataValue: 'endDate' ifAbsent: [^nil])
]

{ #category : #values }
JiraSprint >> goal [
	^self dataValue: 'goal' ifAbsent: [ '' ]
]

{ #category : #'gt-inspector' }
JiraSprint >> gtInspectorJiraValuesIn: composite [
	<gtInspectorPresentationOrder: 50>
	
	^ composite table
		title: 'Jira';
		display: [ {  
			'Name' -> (self name).
			'Goal' -> (self goal).	
			'Status' -> (self status).	
			'Start Date' -> (self startDate ifNotNil: [:d | d asLocalStringYMDHM]).
			'End Date' -> (self endDate ifNotNil: [:d | d asLocalStringYMDHM]).
			'Completed Date' -> (self completedDate ifNotNil: [:d | d asLocalStringYMDHM]).
			'Duration' -> (self duration).
			
			GTLazyValue named: 'Commitment'  check: [self isPopulated] value: [ self commitment] composite: composite.
			GTLazyValue named: 'Velocity' check: [self isPopulated] value: [ self velocity ] composite: composite.
			'ID' -> (self id).
			GTLazyValue named: 'Issues' check: [self isPopulated] value: [ GTStatsCollection with: self issues evaluating: #velocityPoints ] composite: composite.
			GTLazyValue named: 'UPDATE' lazy: '<doit!>' check: false value:[ self populateIssuesFromJira ] composite: composite.	
			GTLazyValue named: 'RESET' lazy: '<doit!>' check: false value:[ self reset ]	composite: composite.	
									
		} ];
		column: 'Attribute' evaluated: #key;
		column: 'Value' evaluated: #value;
		send: #gtInspectorValue
]

{ #category : #accessing }
JiraSprint >> interval [
	^ self isPlanned
		ifFalse: [ nil ]
		ifTrue: [ self startDate to: (self completedDate ifNil: [ self endDate ]) ]
]

{ #category : #accessing }
JiraSprint >> isCompleted [
	^self startDate notNil and: [ self endDate notNil and: [self completedDate notNil]]
]

{ #category : #accessing }
JiraSprint >> isPlanned [
	^self startDate notNil and: [ self endDate notNil ]
]

{ #category : #accessing }
JiraSprint >> isPopulated [
	^ issues notNil
]

{ #category : #accessing }
JiraSprint >> issueWithId: anId [
	^self issues detect: [ :i | i id = anId ] ifNone: [ nil ]
]

{ #category : #accessing }
JiraSprint >> issues [
	^ issues ifNil: [ self populateIssuesFromJira ]
]

{ #category : #accessing }
JiraSprint >> issues: anObject [
	issues := anObject
]

{ #category : #internal }
JiraSprint >> populateIssuesFromJira [
	| result |
	
	result := self jira execute: 
		(JiraAgileSprintResource new 
				expand: 'changelog';
				getIssuesForSprint: self id).
		
	self issues: ((result at: 'issues') collect: [ :d | 
		JiraSprintIssue fromSprint: self withData: d ]).
	
	^self issues
]

{ #category : #internal }
JiraSprint >> printIdOn: aStream [
	
	aStream nextPutAll: self name
	
]

{ #category : #accessing }
JiraSprint >> reset [ 
	self issues: nil.
	^nil
]

{ #category : #accessing }
JiraSprint >> startDate [
	^DateAndTime fromString: (self dataValue: 'startDate' ifAbsent: [^nil])
]

{ #category : #values }
JiraSprint >> status [
 ^(self dataValue: 'state') asCamelCase asSymbol 
]

{ #category : #values }
JiraSprint >> velocity [

	^(self issues collect: [ :i | i velocityPoints ]) sumNumbers
	
	
]
