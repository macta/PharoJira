Class {
	#name : #JiraQuery,
	#superclass : #JiraItem,
	#instVars : [
		'issues',
		'resource',
		'name',
		'type'
	],
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
JiraQuery class >> named: aString resource: jiraResource fromJira: aJira [
	^(self withJira: aJira andData: Dictionary new)
		resource: jiraResource;
		name: aString;
		yourself
		
]

{ #category : #'instance creation' }
JiraQuery class >> resource: jiraResource fromJira: aJira [
	^(self withJira: aJira andData: Dictionary new)
		resource: jiraResource;
		yourself
		
]

{ #category : #'instance creation' }
JiraQuery class >> resource: jiraResource fromJira: aJira withData: aDictionary [
	^(self withJira: aJira andData: aDictionary)
		resource: jiraResource;
		yourself
		
]

{ #category : #accessing }
JiraQuery >> averageStorySize [
	| points |
	points := self issues collect: [ :i | i totalStoryPoints ] thenReject: [ :i | i = 0 ].
	points ifEmpty: [  ^0  ].
	^ points sumNumbers / (points size)
]

{ #category : #'gt-inspector' }
JiraQuery >> gtInspectorJiraValuesIn: composite [
	<gtInspectorPresentationOrder: 50>
	
	^ composite table
		title: 'Jira';
		display: [ {  
			'Name' -> (self name).
			'Type' -> (self type).
			GTLazyValue named: 'Size' check: [ self hasPopulatedIssues ] value: [ self issues size ] composite: composite.
			GTLazyValue named: 'Issues' check: [ self hasPopulatedIssues ] value: [ self issues ] composite: composite.
			GTLazyValue named: 'Avg Points' check: [ self hasPopulatedIssues ] value: [ self averageStorySize ] composite: composite.
			GTLazyValue named: 'Total Points' check: [ self hasPopulatedIssues ] value: [ self totalStoryPoints ] composite: composite.
			GTLazyValue named: 'UPDATE' lazy: '<doit!>' check: false value:[ self populateIssuesFromJira ] composite: composite.		
			GTLazyValue named: 'RESET' lazy: '<doit!>' check: false value:[ self reset ] composite: composite.				
		} ];
		column: 'Attribute' evaluated: #key;
		column: 'Value' evaluated: #value;
		send: #gtInspectorValue
]

{ #category : #accessing }
JiraQuery >> hasPopulatedIssues [
	^issues notNil
	
]

{ #category : #accessing }
JiraQuery >> issues [
	^issues ifNil: [ self populateIssuesFromJira ]
]

{ #category : #accessing }
JiraQuery >> issues: anObject [
	^issues := anObject
]

{ #category : #querying }
JiraQuery >> itemMatching: aRegexString [

	| regex |
	regex := RxMatcher forString: aRegexString.
	^ self issues detect: [ :s | regex matches: s name ]
]

{ #category : #querying }
JiraQuery >> itemNamed: aName [
	^ self issues detect: [ :s | s name = aName ]
]

{ #category : #accessing }
JiraQuery >> jiraId [
	^self data at: 'id'
]

{ #category : #accessing }
JiraQuery >> name [
	^ name ifNil: [ super name ]
]

{ #category : #accessing }
JiraQuery >> name: anObject [
	name := anObject
]

{ #category : #internal }
JiraQuery >> populateIssuesFromJira [
	| result  |
		 
	result := self jira
		execute: self resource
		injecting: OrderedCollection new
		into: [ :finalResult :queryResult | 
			finalResult 	addAll:
					((queryResult at: 'issues') collect: [ :s | JiraVersionIssue withJira: self jira andData: s ]).
			finalResult ].
		
	self issues: result.
	^ result
]

{ #category : #querying }
JiraQuery >> printIdOn: aStream [
	
	aStream nextPutAll: self type, ': ', self name
	
]

{ #category : #accessing }
JiraQuery >> reset [ 
	self issues: nil.
	^nil
]

{ #category : #accessing }
JiraQuery >> resource [
	^ resource
]

{ #category : #accessing }
JiraQuery >> resource: anObject [
	resource := anObject
]

{ #category : #accessing }
JiraQuery >> totalStoryPoints [
	| points |
	points := self issues collect: [ :i | i totalStoryPoints ].
	^ points sum 
]

{ #category : #accessing }
JiraQuery >> type [
	^ type
]

{ #category : #accessing }
JiraQuery >> type: anObject [
	type := anObject
]
