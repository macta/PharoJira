Extension { #name : #JiraSettings }

{ #category : #'*JiraWrappers' }
JiraSettings class >> url: stringUrl username: stringUsername password: stringPassword [ 
	^ self new
		username: stringUsername;
		password: stringPassword;
		jiraUrl: stringUrl;
		yourself
]
