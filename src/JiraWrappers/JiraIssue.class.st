"
I represent a Jira Issue, a Jira concept that is a work item (in Agile terms, this is an alias for a User Story).

When using Jira Agile, there is a ""storyPoints"" value that is a relative estimate for the cost of that story.

TODO: neeed to query the user field for storypoints 
e.g. 

	r := JiraApiFieldResource new.
	res := jira execute: r getFields.
	res detect: [ :f | (f at: 'name') = 'Story Points' ]
"
Class {
	#name : #JiraIssue,
	#superclass : #JiraItem,
	#instVars : [
		'sprints'
	],
	#category : #JiraWrappers
}

{ #category : #comparing }
JiraIssue >> = anObject [
	"Answer whether the receiver and anObject represent the same object."

	self == anObject
		ifTrue: [ ^ true ].
	self class = anObject class
		ifFalse: [ ^ false ].
	^self id = anObject id
]

{ #category : #values }
JiraIssue >> allStoryPoints [
	^(self sprints collect: [ :s | (s issueWithId: self id) ifNil: [ 0 ] ifNotNil: [:item | item storyPoints ]]) 
]

{ #category : #values }
JiraIssue >> completedSprints [

	"Newer versions seem to also have a field 'closedSprints'"
				
	^(self sprintData 
				select: [ :s |
					(s at: 'state') asLowercase = 'closed'] 
				thenCollect: [ :s | |sid|
					sid := (s at:'id').
					JiraSprint fromJira: self jira withId: sid ]) asOrderedCollection 
]

{ #category : #accessing }
JiraIssue >> component [
	^((self dataValue: 'components') first) at: 'name'
]

{ #category : #internal }
JiraIssue >> dataValues [

	^(self data at: 'fields')
]

{ #category : #accessing }
JiraIssue >> description [
	^(self dataValue: 'description')
]

{ #category : #values }
JiraIssue >> finalSprint [
	^self scheduledSprints ifNil: [ nil ] ifNotNil: [ :s | s last ]
]

{ #category : #values }
JiraIssue >> finalStatus [
	^((self dataValue: 'status') at: 'name') asCamelCase asSymbol 
]

{ #category : #values }
JiraIssue >> finalStoryPoints [
	^self dataValue: 'customfield_10014' default: 0
]

{ #category : #internal }
JiraIssue >> firstPlanned [
	^self sprints ifEmpty: [ nil ] ifNotEmpty: [:s | s first startDate ]
]

{ #category : #'gt-inspector' }
JiraIssue >> gtAttrributes: composite [
	^ {('Jira ID' -> self jiraId).
	('Title' -> self title).
	('Status' -> self status).
	('Planned Points' -> self storyPoints).
	GTLazyValue named: 'Total Points' check: [ self hasPopulatedSprints ] value: [ self totalStoryPoints ] composite: composite.
	('# Sprints' -> self sprintCount).
	('Versions' -> self versions).
	('ID' -> self id).
	GTLazyValue named: 'Data' check: [ false ] value: [ self dataValues  ].
	GTLazyValue named: 'Browse Jira' lazy: self jiraUrl check: [ false ] value: [ self openInWebBrowser  ].
	GTLazyValue named: 'UPDATE' lazy: '<doit!>' check: [ false ] value: [ self sprints. nil  ].
	GTLazyValue named: 'Sprints' check: [ self hasPopulatedSprints ] value: [ self sprints ] composite: composite.
	GTLazyValue named: 'History' lazy: '...' check: [ false ] value: [ self historyItems ].
	GTLazyValue named: 'Raw History' lazy: '...' check: [ false ] value: [ self history ]
	 }
]

{ #category : #'gt-inspector' }
JiraIssue >> gtInspectorJiraValuesIn: composite [
	<gtInspectorPresentationOrder: 50>
	^ composite table
		title: 'Jira';
		display: [ self gtAttrributes: composite ];
		column: 'Attribute' evaluated: #key;
		column: 'Value' evaluated: #value;
		send: #gtInspectorValue
]

{ #category : #values }
JiraIssue >> hasPopulatedSprints [
	^ sprints notNil 
]

{ #category : #comparing }
JiraIssue >> hash [
	"Answer an integer value that is related to the identity of the receiver."

	^self id hash
]

{ #category : #values }
JiraIssue >> history [
	^((self data at: 'changelog') at: 'histories')  
]

{ #category : #internal }
JiraIssue >> historyForAttribute: aName [
	^ self history
		inject: OrderedCollection new
		into: [ :result :value | 
			(value at: 'items')
				do: [ :i | 
					(i at: 'field') = aName
						ifTrue: [ result add: value ] ].
			result ]
]

{ #category : #internal }
JiraIssue >> historyForAttribute: aName over: aTimespan ifAbsent: aBlock [
	
	(self historyForAttribute: aName) do: [ :item |
		(aTimespan end > (DateAndTime fromString: (item at: 'created')))
			ifTrue: [ 
				(item at: 'items') do: [ :v | (v at: 'field') = aName ifTrue: [ ^v at: 'toString']]]].
			
	^aBlock value
]

{ #category : #accessing }
JiraIssue >> historyItems [ 
	^self history collect: [ :h | JiraHistory withJira: self jira andData: h]
]

{ #category : #testing }
JiraIssue >> isComplete [
	^self status = #Done
]

{ #category : #accessing }
JiraIssue >> jira [ 
	^jira
]

{ #category : #internal }
JiraIssue >> jiraId [
	^self data at: 'key'
]

{ #category : #internal }
JiraIssue >> jiraUrl [
	^ self jira jiraUrl , '/browse/' , self jiraId
]

{ #category : #internal }
JiraIssue >> openInWebBrowser [
	WebBrowser openOn: self jiraUrl
]

{ #category : #values }
JiraIssue >> printIdOn: aStream [
	
	aStream nextPutAll: self jiraId
]

{ #category : #values }
JiraIssue >> scheduledSprints [
	"Newer versions seem to also have a field 'closedSprint'"
				
	^(self sprintData 
				reject: [ :s |
					(s at: 'state') asLowercase  = 'future'] 
				thenCollect: [ :s | |sid|
					sid := (s at:'id').
					JiraSprint fromJira: self jira withId: sid ]) sorted: [ :s1 :s2 | s1 endDate < s2 endDate ] 
]

{ #category : #values }
JiraIssue >> sprint [ 
	^self subclassResponsibility 
]

{ #category : #values }
JiraIssue >> sprintCount [
	^ (self sprintData reject: [ :s | (s at: 'state') asLowercase = 'future' ]) size
]

{ #category : #values }
JiraIssue >> sprintData [
	"Answer the sprint data value"
	| r |
	r := self dataValue: 'customfield_10010' ifAbsent: [^#()].
	r isNil ifTrue: [ ^#() ].
	r first isString ifFalse: [  ^r ].
	
	^r collect: [ :s | 
			(s substrings: '[,]') allButFirst inject: Dictionary new into: [:res :l |
				((l substrings: '=' ) pairsDo: [:a :b| res add: a -> b ]). res ] ] 
]

{ #category : #values }
JiraIssue >> sprints [
	^ sprints ifNil: [ self sprints: self scheduledSprints ]
]

{ #category : #accessing }
JiraIssue >> sprints: anObject [
	^sprints := anObject
]

{ #category : #values }
JiraIssue >> status [
	^ self timespan ifNil: [ self finalStatus ]
		ifNotNil: 
			[ :t | (self historyForAttribute: 'status' over: t ifAbsent: [ 'ToDo' ])
				asCamelCase asSymbol ]
]

{ #category : #values }
JiraIssue >> storyPoints [
	"If you re-estimate stories at the end of your sprint, in preparation for the next one,
	you need to adjust the interval where you look for story point modifications"
	
	^ self timespan
		ifNil: [ self finalStoryPoints ]
		ifNotNil: [ :t | self storyPointsAt: t - 1 day ]
]

{ #category : #internal }
JiraIssue >> storyPointsAt: aTimespan [
	| result |
	
	result := self
		historyForAttribute: 'Story Points'
		over: aTimespan
		ifAbsent: [ ^ self finalStoryPoints ].
		
	^ [result asNumber] on: Error do: [ :e | 0 ]
]

{ #category : #accessing }
JiraIssue >> timespan [
	^self sprint ifNil: [ nil] ifNotNil: [:s | s interval ]
]

{ #category : #accessing }
JiraIssue >> title [
	^(self dataValue: 'summary')
]

{ #category : #values }
JiraIssue >> totalStoryPoints [
	^self allStoryPoints sumNumbers 
			 
]

{ #category : #accessing }
JiraIssue >> type [
	^(self dataValue: 'issuetype') at: 'name'
]

{ #category : #values }
JiraIssue >> velocityPoints [
	^self isComplete
		ifTrue: [ self storyPoints ]
		ifFalse: [ 0 ]
]

{ #category : #versions }
JiraIssue >> version [
	^self versions detect: [:v | true ] ifNone: [ '' ]
]

{ #category : #values }
JiraIssue >> versions [
	^(self dataValue: 'fixVersions') collect: [ :i | i at: 'name']
]
