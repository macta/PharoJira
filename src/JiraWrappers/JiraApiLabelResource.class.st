Class {
	#name : #JiraApiLabelResource,
	#superclass : #JiraApiResource,
	#category : #JiraWrappers
}

{ #category : #'resource-methods' }
JiraApiLabelResource >> getLabels [
	self resource: self resourceBase, '/label'.
	self beGet.


]
