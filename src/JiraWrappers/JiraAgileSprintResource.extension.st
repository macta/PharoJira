Extension { #name : #JiraAgileSprintResource }

{ #category : #'*JiraWrappers' }
JiraAgileSprintResource >> getIssuesForSprint: aSprintId [
	self one: aSprintId.
	self resource: self resource, '/issue'.
	self beGet
]
