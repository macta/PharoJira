"
I represent a Jira Workspace, a top level item that groups Boards and projects.
"
Class {
	#name : #JiraWorkspace,
	#superclass : #Object,
	#instVars : [
		'jira',
		'boards',
		'projects'
	],
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
JiraWorkspace class >> fromJira: aJira [
	
	^ self new
		jira: aJira;
		yourself
		
]

{ #category : #'instance creation' }
JiraWorkspace class >> withSettings: aJiraSettings [
	
	^ self fromJira: (CachedJira fromSettings: aJiraSettings )
]

{ #category : #accessing }
JiraWorkspace >> boardNamed: aName [
	^ JiraBoard fromJira: self jira named:  aName
]

{ #category : #accessing }
JiraWorkspace >> boards [
	^ boards ifNil: [ self boards: (JiraBoard allFromJira: self jira) ]
]

{ #category : #accessing }
JiraWorkspace >> boards: aCollection [
	^boards := aCollection 
]

{ #category : #accessing }
JiraWorkspace >> fields [ 
	| res |
	res := self jira execute: JiraApiFieldResource new getFields.
	"res detect: [ :f | (f at: 'name') = 'Story Points' ]"
	^res
]

{ #category : #accessing }
JiraWorkspace >> jira [
	^ jira
]

{ #category : #accessing }
JiraWorkspace >> jira: anObject [
	jira := anObject
]

{ #category : #accessing }
JiraWorkspace >> labels [
	| result |
	result := self jira execute: JiraApiLabelResource new getLabels.
	"res detect: [ :f | (f at: 'name') = 'Story Points' ]"
	^ (result at: 'values')
		collect: [ :label | 
			(JiraQuery
				resource:
					(JiraApiSearchResource new
						search: 'labels="' , label , '"';
						expand: 'changelog';
						yourself)
				fromJira: self jira) 
					name: label; 
					type: 'Label'; 
					yourself ]
]

{ #category : #accessing }
JiraWorkspace >> projectWithKey: key [
	^ JiraProject fromJira: self jira withKey: key
]

{ #category : #accessing }
JiraWorkspace >> projects [
	^ projects ifNil: [ self projects: (JiraProject allFromJira: self jira) ]
]

{ #category : #accessing }
JiraWorkspace >> projects: anObject [
	^projects := anObject
]

{ #category : #accessing }
JiraWorkspace >> reset [
	self
		projects: nil;
		boards: nil.
	self jira reset
]

{ #category : #accessing }
JiraWorkspace >> sprintWithId: id [

	^JiraSprint fromJira: self jira withId: id
]
