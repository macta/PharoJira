Class {
	#name : #JiraHistory,
	#superclass : #JiraItem,
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
JiraHistory class >> fromProject: aProject withData: aJiraDictionary [
	^ self withJira: aProject jira andData: aJiraDictionary
]

{ #category : #'instance creation' }
JiraHistory class >> named: aName withJira: aJira withData: aJiraDictionary [
	^ (self withJira: aJira andData: aJiraDictionary)
		name: aName;
		yourself
]

{ #category : #'gt-inspector' }
JiraHistory >> dataValues [

	^(self data at: 'items') last
]

{ #category : #'gt-inspector' }
JiraHistory >> date [ 	
	^self data at: #created
]

{ #category : #'gt-inspector' }
JiraHistory >> from [ 	
	^self dataValue: 'fromString' ifAbsent: ['']
]

{ #category : #'gt-inspector' }
JiraHistory >> gtInspectorJiraValuesIn: composite [
	<gtInspectorPresentationOrder: 50>
	
	^ composite table
		title: 'Jira';
		display: [ {  
			'Name' -> (self name).
			'Date' -> (self date).
			'From' -> (self from).
			'To' -> (self to).
		} ];
		column: 'Attribute' evaluated: #key;
		column: 'Value' evaluated: #value;
		send: #gtInspectorValue
]

{ #category : #'gt-inspector' }
JiraHistory >> name [ 	
	^self dataValue: 'field' ifAbsent: ['']
]

{ #category : #'gt-inspector' }
JiraHistory >> printIdOn: aStream [
	
	aStream nextPutAll: self name
	
]

{ #category : #'gt-inspector' }
JiraHistory >> to [ 	
	^self dataValue: 'toString' ifAbsent: [ '' ]
]
