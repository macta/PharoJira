"
I represent a Jira Board, a Jira concept that groups Sprints for a team.

"
Class {
	#name : #JiraBoard,
	#superclass : #JiraItem,
	#instVars : [
		'sprints'
	],
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
JiraBoard class >> allFromJira: aJira [
	| result |
	result := aJira execute: JiraAgileBoardResource new getAllBoards.
	^ (result at: 'values')
		collect: [ :d | self withJira: aJira andData: d ]
		into: OrderedCollection new
]

{ #category : #'instance creation' }
JiraBoard class >> fromJira: aJira named: aName [
	| result |
	result := aJira execute: (JiraAgileBoardResource new getBoardMatching: aName).
	^ self withJira: aJira andData: (result at: 'values') first
]

{ #category : #'gt-inspector' }
JiraBoard >> gtInspectorJiraValuesIn: composite [
	<gtInspectorPresentationOrder: 50>
	
	^ composite table
		title: 'Jira';
		display: [ {  
			'Name' -> (self name).
			'Project Key' -> (self projectKey).		
			GTLazyValue named: 'Sprints' check: [ self hasPopulatedSprints ] value: 
				[ GTStatsCollection with: self sprints evaluating: #velocity ] composite: composite.
			GTLazyValue named: 'RESET' lazy: '<doit!>' check: false value:[ self reset ] composite: composite.		
		} ];
		column: 'Attribute' evaluated: #key;
		column: 'Value' evaluated: #value;
		send: #gtInspectorValue
]

{ #category : #accessing }
JiraBoard >> hasPopulatedSprints [
	^ sprints notNil 
]

{ #category : #querying }
JiraBoard >> printIdOn: aStream [
	
	aStream nextPutAll: self name
	
]

{ #category : #accessing }
JiraBoard >> projectKey [
	^(self dataValue: 'location' default: (Dictionary with: 'projectKey' -> '?')) at: 'projectKey'
]

{ #category : #accessing }
JiraBoard >> reset [
	self sprints: nil.
	nil
]

{ #category : #querying }
JiraBoard >> sprintMatching: aRegexString [

	| regex |
	regex := RxMatcher forString: aRegexString.
	^ self sprints detect: [ :s | regex matches: s name ]
]

{ #category : #querying }
JiraBoard >> sprintNamed: aName [
	^ self sprints detect: [ :s | s name = aName ]
]

{ #category : #accessing }
JiraBoard >> sprints [
	^ sprints ifNil: [ [self sprintsFromJira] on: Error do: [ #() ] ]
]

{ #category : #accessing }
JiraBoard >> sprints: anObject [
	sprints := anObject
]

{ #category : #internal }
JiraBoard >> sprintsFromJira [
	| result sprintQuery |
	
	sprintQuery := JiraAgileBoardSprintResource new
		getSprintsOfBoardId: self id;
		startAt: 0;
		maxResults: 50.
		 
	result := self jira
		execute: sprintQuery
		injecting: OrderedCollection new
		into: [ :finalResult :queryResult | 
			finalResult 	addAll:
					((queryResult at: 'values') collect: [ :s | JiraSprint withJira: self jira andData: s ]).
			finalResult ].
		
	self sprints: result.
	^ result
]

{ #category : #querying }
JiraBoard >> sprintsIn: aTimeSpan [ 
	"Answer the sprints that start in @aTimeSpan (typically a Mnth)"

	^self sprints select: [ :s | s isPlanned and: [ aTimeSpan includes: s startDate ] ]
]
