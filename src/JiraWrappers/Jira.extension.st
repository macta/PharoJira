Extension { #name : #Jira }

{ #category : #'*JiraWrappers' }
Jira >> environmentVariableNamed: aSymbol [

	^ OSPlatform environment at: aSymbol
	
	
]

{ #category : #'*JiraWrappers' }
Jira >> execute: aJiraQuery [
	| client response  |
	client := self restClient.
	aJiraQuery isGet ifTrue: [ 
		response := client get: self jiraUrl , aJiraQuery asUri.
		(client response code = ZnStatusLine ok code) 
			ifFalse: [ Error signal: 'Cannot retrieve contents ', client response statusLine asString ].
			self logRequestResponseFor: client.
			^ self asDictionary: response.
		].
	aJiraQuery isPost ifTrue: [
		response := client 
				url: self jiraUrl , aJiraQuery asUri;
				contents: aJiraQuery asJson;
				contentType: ZnMimeType applicationJson;
				post. 
		"Sometimes there is just no content sent back"
		({ZnStatusLine created. ZnStatusLine noContent} includes: client response statusLine) "or updated in the future."
			ifFalse: [ client response inspect. Error signal: 'Cannot retrieve contents ', client response statusLine asString ].
		self logRequestResponseFor: client.
		^ self asDictionary: response.
		].
	aJiraQuery isDelete ifTrue: [
		response := client 
				delete: self jiraUrl , aJiraQuery asUri.
		({ZnStatusLine ok. ZnStatusLine noContent. ZnStatusLine accepted} contains: [:each | each = (client response statusLine)])
			ifFalse: [ Error signal: 'Cannot delete ', client response statusLine asString ].
		self logRequestResponseFor: client.
		^ client response statusLine.
		].
	
	aJiraQuery isPut ifTrue: [
		response := client 
				url: self jiraUrl , aJiraQuery asUri;
				contents: aJiraQuery asJson; "<-- if empty, we do not need to send"
				contentType: ZnMimeType applicationJson;
				put.
		({ZnStatusLine ok. ZnStatusLine noContent. ZnStatusLine accepted} contains: [:each | each = (client response statusLine)])
			ifFalse: [ Error signal: 'Cannot put ', client response statusLine asString ].
		self logRequestResponseFor: client.
		^ client response statusLine.
		].
	
	Error signal: 'Unsupported Method'.

	

	

]

{ #category : #'*JiraWrappers' }
Jira >> execute: theQuery injecting: aValue into: aBlock [
	| finalResult startIndex bufferSize queryResult initialIndex aQuery |
	
	aQuery := theQuery deepCopy.
	initialIndex := aQuery options at: #startAt ifAbsent: [0].
	startIndex := initialIndex.
	bufferSize := aQuery options at: #maxResults ifAbsent: [50].
	
	finalResult := aValue.
	
	[  
		queryResult := self execute: aQuery.
		finalResult := aBlock value: finalResult value: queryResult.
		
		queryResult isArray 
			ifTrue: [true ]
			ifFalse: [ 
				startIndex := startIndex + bufferSize.
				aQuery startAt: startIndex.
				
				(queryResult at: 'isLast' ifAbsent: [ 
					(queryResult at: 'total' ifAbsent: [ 0 ]) < startIndex ])]] whileFalse.
	
	^finalResult
]

{ #category : #'*JiraWrappers' }
Jira class >> fromSettings: aJiraSettings [

	^(self new)
			username: aJiraSettings username password: aJiraSettings password;
			domain: aJiraSettings jiraUrl;
			yourself
			
	
]
