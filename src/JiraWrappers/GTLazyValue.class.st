Class {
	#name : #GTLazyValue,
	#superclass : #Object,
	#instVars : [
		'valueBlock',
		'checkBlock',
		'name',
		'lazyValue',
		'composite'
	],
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
GTLazyValue class >> named: aString check: checkBlock value: valueBlock [
	^ self named: aString lazy: '...'  check: checkBlock value: valueBlock
]

{ #category : #'instance creation' }
GTLazyValue class >> named: aString check: checkBlock value: valueBlock composite: gtComposite [
	^ self
		named: aString
		lazy: '...'
		check: checkBlock
		value: valueBlock
		composite: gtComposite
]

{ #category : #'instance creation' }
GTLazyValue class >> named: aString lazy: stringValue check: checkBlock value: valueBlock [
	^ self new
		name: aString;
		checkBlock: checkBlock;
		valueBlock: valueBlock;
		lazyValue: stringValue;
		yourself
]

{ #category : #'instance creation' }
GTLazyValue class >> named: aString lazy: stringValue check: checkBlock value: valueBlock composite: gtComposite [
	^ self new
		name: aString;
		checkBlock: checkBlock;
		valueBlock: valueBlock;
		lazyValue: stringValue;
		composite: gtComposite;
		yourself
]

{ #category : #accessing }
GTLazyValue >> checkBlock [
	^ checkBlock
]

{ #category : #accessing }
GTLazyValue >> checkBlock: anObject [
	checkBlock := anObject
]

{ #category : #accessing }
GTLazyValue >> composite [
	^ composite
]

{ #category : #accessing }
GTLazyValue >> composite: anObject [
	composite := anObject
]

{ #category : #accessing }
GTLazyValue >> computedValue [
	^self valueBlock value
]

{ #category : #accessing }
GTLazyValue >> gtInspectorValue [
	
	^ Cursor wait
		showWhile: [ 
			| result needsRefresh |
			
			needsRefresh := self isComputed not.
			result := self computedValue.
			needsRefresh
				ifTrue: [ self composite ifNotNil: [ :c | [c update ] valueAfterWaiting: 200 milliSeconds asDelay ]].
				
			result ]
]

{ #category : #accessing }
GTLazyValue >> isComputed [
	^self checkBlock value
]

{ #category : #accessing }
GTLazyValue >> key [
	^self name
]

{ #category : #accessing }
GTLazyValue >> lazyValue [
	^lazyValue 
]

{ #category : #accessing }
GTLazyValue >> lazyValue: anObject [
	lazyValue := anObject
]

{ #category : #accessing }
GTLazyValue >> name [
	^ name
]

{ #category : #accessing }
GTLazyValue >> name: anObject [
	name := anObject
]

{ #category : #accessing }
GTLazyValue >> printOn: aStream [
	aStream
		nextPutAll: self name;
		nextPutAll: '->'.
	self isComputed
		ifTrue: [ self computedValue printOn: aStream ]
		ifFalse: [ aStream nextPutAll: self lazyValue ]
]

{ #category : #accessing }
GTLazyValue >> value [ 
	^self isComputed ifTrue: [ self gtInspectorValue ] ifFalse: [ self lazyValue ]
]

{ #category : #accessing }
GTLazyValue >> valueBlock [
	^ valueBlock
]

{ #category : #accessing }
GTLazyValue >> valueBlock: anObject [
	valueBlock := anObject
]
