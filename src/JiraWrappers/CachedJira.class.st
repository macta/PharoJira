Class {
	#name : #CachedJira,
	#superclass : #Jira,
	#instVars : [
		'queryCache',
		'ttlDuration',
		'cacheLimit'
	],
	#category : #JiraWrappers
}

{ #category : #accessing }
CachedJira >> cacheLimit [
	^ cacheLimit
]

{ #category : #accessing }
CachedJira >> cacheLimit: anObject [
	cacheLimit := anObject
]

{ #category : #'*JiraWrappers' }
CachedJira >> execute: aJiraQuery [
	^ self queryCache
		at: aJiraQuery hash
		ifAbsentPut: [ 
			Transcript logd: aJiraQuery printString.
			super execute: aJiraQuery ]
]

{ #category : #accessing }
CachedJira >> initialize [
	super initialize.
	self ttlDuration: 1 day.
	self cacheLimit: 128.
]

{ #category : #accessing }
CachedJira >> queryCache [
	^ queryCache
		ifNil: [ self
				queryCache:
					(TTLCache new
						timeToLive: self ttlDuration;
						maximumWeight: self cacheLimit;
						yourself) ]
]

{ #category : #accessing }
CachedJira >> queryCache: anObject [
	^queryCache := anObject
]

{ #category : #accessing }
CachedJira >> reset [ 
	
	self queryCache: nil.
]

{ #category : #accessing }
CachedJira >> ttlDuration [
	^ ttlDuration
]

{ #category : #accessing }
CachedJira >> ttlDuration: anObject [
	ttlDuration := anObject
]
