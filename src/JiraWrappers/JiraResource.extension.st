Extension { #name : #JiraResource }

{ #category : #'*JiraWrappers' }
JiraResource >> = anObject [
	"Answer whether the receiver and anObject represent the same object."

	self == anObject
		ifTrue: [ ^ true ].
	self class = anObject class
		ifFalse: [ ^ false ].
	^ isUsingJsonObject = anObject isUsingJsonObject
		and: [ options = anObject options
				and: [ resourceBase = anObject resourceBase
						and: [ resourceName = anObject resourceName ] ] ]
]

{ #category : #'*JiraWrappers' }
JiraResource >> hash [
	"Answer an integer value that is related to the identity of the receiver."

	^ isUsingJsonObject hash
		bitXor: (options hash bitXor: (resourceBase hash bitXor: resourceName hash))
]

{ #category : #'*JiraWrappers' }
JiraResource >> printOn: stream [
	super printOn: stream.
	stream nextPut: $(.
	stream nextPutAll: self resource.
	stream nextPut: $)
]
