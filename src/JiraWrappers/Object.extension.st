Extension { #name : #Object }

{ #category : #'*JiraWrappers' }
Object >> gtInspectorValue [
	"Answer an object suitable to follow, when clicked in the inspector. This allows cached or proxy objects to materialise"
	
	^self 
]

{ #category : #'*JiraWrappers' }
Object >> logd: aString [
	"Log the argument. Use self log: instead of Transcript show: "
	
	| d ts |
	d := DateAndTime now.
	ts := String streamContents: [ :s | d printYMDOn: s. s space. d printHMSOn: s]. 
	Transcript cr; show: ts; tab; show: aString.
]
