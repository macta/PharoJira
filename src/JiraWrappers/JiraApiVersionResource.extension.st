Extension { #name : #JiraApiVersionResource }

{ #category : #'*JiraWrappers' }
JiraApiVersionResource >> getAll [
	self resource: self resourceBase, ('/{1}' format: { self resourceName pluralize }).
	self beGet.
]
