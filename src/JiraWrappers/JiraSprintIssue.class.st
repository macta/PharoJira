Class {
	#name : #JiraSprintIssue,
	#superclass : #JiraIssue,
	#instVars : [
		'sprint'
	],
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
JiraSprintIssue class >> fromSprint: aSprint withData: aJiraDictionary [
	^ (self withJira: aSprint jira andData: aJiraDictionary)
		sprint: aSprint;
		yourself
]

{ #category : #accessing }
JiraSprintIssue >> gtAttrributes: anObject [
	| composite |
	^ (super gtAttrributes: composite), {
	('Sprint' -> self sprint).
	('Timespan' -> self timespan).
	  
	}
]

{ #category : #accessing }
JiraSprintIssue >> sprint [
	^sprint
]

{ #category : #accessing }
JiraSprintIssue >> sprint: anObject [
	sprint := anObject
]
