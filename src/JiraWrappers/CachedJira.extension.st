Extension { #name : #CachedJira }

{ #category : #'*JiraWrappers' }
CachedJira >> execute: aJiraQuery [
	^ self queryCache
		at: aJiraQuery hash
		ifAbsentPut: [ 
			Transcript logd: aJiraQuery printString.
			super execute: aJiraQuery ]
]
