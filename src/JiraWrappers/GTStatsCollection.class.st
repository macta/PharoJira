Class {
	#name : #GTStatsCollection,
	#superclass : #Object,
	#instVars : [
		'collection',
		'valueBlock'
	],
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
GTStatsCollection class >> with: aCollection [
	^ self new
		collection: aCollection;
		yourself
]

{ #category : #'instance creation' }
GTStatsCollection class >> with: aCollection evaluating: aBlock [
	^ (self with: aCollection)
		valueBlock: aBlock;
		yourself
]

{ #category : #printing }
GTStatsCollection >> avg [
	^ (self collection collect: [:i | self valueBlock ifNil: [ 0] ifNotNil: [:v | v value: i ]]) average
]

{ #category : #accessing }
GTStatsCollection >> collection [
	^ collection
]

{ #category : #accessing }
GTStatsCollection >> collection: anObject [
	collection := anObject
]

{ #category : #printing }
GTStatsCollection >> gtInspectorItemsIn: composite [
	<gtInspectorPresentationOrder: 0>
	
	self collection gtInspectorItemsIn: composite.
	
	^composite table
		title: 'Stats';
		display: [ {  
			'Avg' -> (self avg).
			'Min' -> (self min).	
			'Max' -> (self max) }];
		column: 'Attribute' evaluated: #key;
		column: 'Value' evaluated: #value;
		send: #gtInspectorValue.
				

]

{ #category : #printing }
GTStatsCollection >> isEmpty [ 	
	^self size = 0
]

{ #category : #printing }
GTStatsCollection >> max [
	^ self values max
]

{ #category : #printing }
GTStatsCollection >> min [
	^ (self collection collect: [:i | self valueBlock ifNil: [ 0] ifNotNil: [:v | v value: i ]]) min
]

{ #category : #printing }
GTStatsCollection >> printOn: aStream [
	self size printOn: aStream.
	
	aStream
		space;
		nextPutAll: 'Item'.
		
	self size > 1
		ifTrue: [ aStream nextPut: $s ].
		
	aStream nextPutAll: ': '.
	self collection printElementsOn: aStream
]

{ #category : #printing }
GTStatsCollection >> size [
	^ self collection size
]

{ #category : #accessing }
GTStatsCollection >> valueBlock [
	^ valueBlock
]

{ #category : #accessing }
GTStatsCollection >> valueBlock: anObject [
	valueBlock := anObject
]

{ #category : #printing }
GTStatsCollection >> values [
	^self isEmpty ifTrue: [ { 0}  ] ifFalse: [ 
	 self collection
		collect: [ :i | self valueBlock ifNil: [ 0 ] ifNotNil: [ :v | v value: i ] ]]
]
