Class {
	#name : #JiraVersionIssue,
	#superclass : #JiraIssue,
	#category : #JiraWrappers
}

{ #category : #values }
JiraVersionIssue >> sprint [
	^self sprints ifEmpty: [ nil ] ifNotEmpty: [ :s | s last ]
]
