Extension { #name : #JiraApiSearchResource }

{ #category : #'*JiraWrappers' }
JiraApiSearchResource >> printOn: stream [
	super printOn: stream.
	stream nextPut: $[.
	stream nextPutAll: (self optionAt: #jql).
	stream nextPut: $]
]
