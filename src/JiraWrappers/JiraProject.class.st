"
I represent a JiraProject, identified by a key prefix that is spefied in Jira (e.g. the prefix that is added to all issue names).

To interact with Jira, you would do something like the following:

JiraSettings 
	jiraUrl: 'company.atlassian.net';
	jiraUsername: 'user@company.com';
	jiraPassword: '7v96XrajEp9xsHa0bCqyF4110'.
	
jira := Jira fromSettings.

project := JiraProject fromJira: jira key: 'DEV'.
project board sprints.
project versions detect: [ :v | v name = 'My Project' ].
"
Class {
	#name : #JiraProject,
	#superclass : #JiraItem,
	#instVars : [
		'versions',
		'boards'
	],
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
JiraProject class >> allFromJira: aJira [

	| result |
	result := aJira execute: (JiraApiProjectResource new getAll).
	
	^result collect: [:d |
		self withJira: aJira andData: d ]
]

{ #category : #'instance creation' }
JiraProject class >> fromJira: aJira withKey: prefixName [
	| result |
	
	result := aJira
		execute: (JiraApiProjectResource new getProject: prefixName).
		
	^ self withJira: aJira andData: result
]

{ #category : #accessing }
JiraProject >> boards [
	^ boards ifNil: [ self boardsFromJira ]
]

{ #category : #accessing }
JiraProject >> boards: anObject [
	boards := anObject
]

{ #category : #internal }
JiraProject >> boardsFromJira [
	| result |
	
	result := ((JiraBoard allFromJira: self jira) select: [ :b | b projectKey = self key ]).
	self boards: result.
	^result
	
]

{ #category : #'gt-inspector' }
JiraProject >> gtInspectorJiraValuesIn: composite [
	<gtInspectorPresentationOrder: 50>
	
	^ composite table
		title: 'Jira';
		display: [ {  
			'Name' -> (self name).
			GTLazyValue named: 'Versions' check: [ self hasPopulatedVersions ] value: [ GTStatsCollection with: self versions ] composite: composite.	
			GTLazyValue named: 'RESET' lazy: '<doit!>' check: false value:[ self reset ] composite: composite.		
		} ];
		column: 'Attribute' evaluated: #key;
		column: 'Value' evaluated: #value;
		send: #gtInspectorValue
]

{ #category : #'gt-inspector' }
JiraProject >> hasPopulatedVersions [ 
 	^versions notNil
]

{ #category : #'gt-inspector' }
JiraProject >> key [ 
	^self dataValue: 'key'
]

{ #category : #internal }
JiraProject >> populateVersionsFromJira [
	| result  versionsQuery |
	
	versionsQuery := JiraApiProjectResource new
		getProjectVersions: self id. 
		 
	result := self jira
		execute: versionsQuery
		injecting: OrderedCollection new
		into: [ :finalResult :queryResult | 
			finalResult 	addAll:
					(queryResult collect: [ :v | 
						(JiraQuery 
							resource: (JiraApiSearchResource new
								search: 'fixVersion="' , (v at: 'name'), '"';
								expand: 'changelog';
								yourself)
							fromJira: self jira
							withData: v) 
								type: 'Version'; 
								yourself
						]).
			finalResult ]. 
		
	self versions: result.
	^result
	
]

{ #category : #'gt-inspector' }
JiraProject >> printIdOn: aStream [
	
	aStream nextPutAll: self key, ' : ', self name
	
]

{ #category : #'gt-inspector' }
JiraProject >> reset [ 
 	^versions := nil
]

{ #category : #accessing }
JiraProject >> sprints [
	^ self boards inject: Set new into: [ :r :b | r addAll: b sprints; yourself ]
]

{ #category : #accessing }
JiraProject >> versions [
	^ versions ifNil: [ self populateVersionsFromJira ] 
]

{ #category : #accessing }
JiraProject >> versions: anObject [
	versions := anObject
]
