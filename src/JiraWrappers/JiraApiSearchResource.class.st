Class {
	#name : #JiraApiSearchResource,
	#superclass : #JiraApiResource,
	#category : #JiraWrappers
}

{ #category : #'*JiraWrappers' }
JiraApiSearchResource >> printOn: stream [
	super printOn: stream.
	stream nextPut: $[.
	stream nextPutAll: (self optionAt: #jql).
	stream nextPut: $]
]

{ #category : #'typing/selecting keys' }
JiraApiSearchResource >> search: jqlQuery [
	"Note that you need to have the privilege given in Jira for this to work."
	self resource: self resourceBase, '/search'.
	self optionAt: #jql put: jqlQuery.
	self beGet.


]
