"
I represent higer level Jira model items that contain the raw json data that the Jira REST API defines.

I am an abstract class.
"
Class {
	#name : #JiraItem,
	#superclass : #Object,
	#instVars : [
		'data',
		'id',
		'jira'
	],
	#category : #JiraWrappers
}

{ #category : #'instance creation' }
JiraItem class >> withJira: aJira andData: data [

	^ self new
		jira: aJira;
		data: data;
		yourself
]

{ #category : #accessing }
JiraItem >> data [
	^ data
]

{ #category : #accessing }
JiraItem >> data: anObject [
	data := anObject
]

{ #category : #accessing }
JiraItem >> dataValue: aKey [

	^self dataValue: aKey default: [ self error: 'missing value for ', aKey ]
]

{ #category : #accessing }
JiraItem >> dataValue: aKey default: aValue [
	^(self dataValue: aKey ifAbsent: [ aValue ]) ifNil: [ aValue ] 
]

{ #category : #accessing }
JiraItem >> dataValue: aKey ifAbsent: aBlock [

	^self dataValues at: aKey ifAbsent: aBlock
]

{ #category : #accessing }
JiraItem >> dataValues [
	^self data
]

{ #category : #accessing }
JiraItem >> id [
	^ id ifNil: [ self id: (self data at: 'id' ifAbsent: ['?']) asString ].
	
]

{ #category : #accessing }
JiraItem >> id: anObject [
	^id := anObject
]

{ #category : #accessing }
JiraItem >> jira [
	^ jira
]

{ #category : #accessing }
JiraItem >> jira: anObject [
	jira := anObject
]

{ #category : #accessing }
JiraItem >> name [
	^self dataValue: 'name'
]

{ #category : #internal }
JiraItem >> printIdOn: aStream [
	
	aStream nextPutAll: self id
	
]

{ #category : #internal }
JiraItem >> printOn: aStream [
	super printOn: aStream.
	aStream nextPut: $(.
	self printIdOn: aStream.
	aStream nextPut: $)
]

{ #category : #accessing }
JiraItem >> restUrl [
 ^self data at: 'self' 
]
