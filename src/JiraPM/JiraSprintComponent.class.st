Class {
	#name : #JiraSprintComponent,
	#superclass : #JiraComponent,
	#category : #'JiraPM-Seaside'
}

{ #category : #rendering }
JiraSprintComponent >> goBack [
	self answer: false.
]

{ #category : #rendering }
JiraSprintComponent >> renderContact: s on: html [
	^ html render: s name
]

{ #category : #rendering }
JiraSprintComponent >> renderContentOn: html [
	html heading: 'Jira Reporter'.
	html heading
		level: 3;
		with: 'Sprints'.
	html
		orderedList: [ self session board sprints
				do: [ :s | html listItem: [ self renderContact: s on: html ] ] ].
			
	self renderButton: 'Previous' sending: #goBack on: html.
]
