Extension { #name : #JiraSprint }

{ #category : #'*JiraPM' }
JiraSprint >> costedIssues [
	^ self issues reject: [ :i | i status = #ToDo ]
]
