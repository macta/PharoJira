Class {
	#name : #JiraSeasideSession,
	#superclass : #WASession,
	#instVars : [
		'jira',
		'board'
	],
	#category : #'JiraPM-Seaside'
}

{ #category : #accessing }
JiraSeasideSession >> board [
	^board
]

{ #category : #accessing }
JiraSeasideSession >> jira [
	^jira 
]

{ #category : #'as yet unclassified' }
JiraSeasideSession >> login: aJira [
	
	| boards |
	jira := aJira.
	boards := (JiraWorkspace fromJira: jira) boards.
	board := boards detect: [ :b | b name = 'DEV board' ] ifNone: [nil].
]

{ #category : #'as yet unclassified' }
JiraSeasideSession >> unregistered [
     super unregistered.
     "tear down anything we need to"
]
