Class {
	#name : #JiraStatsComponent,
	#superclass : #JiraComponent,
	#instVars : [
		'date'
	],
	#category : #'JiraPM-Seaside'
}

{ #category : #accessing }
JiraStatsComponent >> date [
	^ date
]

{ #category : #accessing }
JiraStatsComponent >> date: anObject [
	date := anObject
]

{ #category : #rendering }
JiraStatsComponent >> goBack [ 
	self answer: true
]

{ #category : #rendering }
JiraStatsComponent >> renderContentOn: html [
	html heading: 'Jira Stats Reporter'.
	html heading
		level: 3;
		with: 'Sprint Stats for: ', self date month printString.
		
	html preformatted: [ 
		self renderCsvStatsOn: html ].
		
	self renderButton: 'Previous' sending: #goBack on: html.
	
]

{ #category : #rendering }
JiraStatsComponent >> renderCsvStatsOn: html [
	| monthIssues csv |
	
	monthIssues := self selectIssues.
	
	csv := String
		streamContents: [ :stream | 
			(NeoCSVWriter on: stream)
				nextPut: #('ID' 'Title' 'Status' 'Points' 'LOB' 'Sprint');
				addFields:
					{ #id. #title. #status. #storyPoints. #version. [ :i | i sprint name ] };
				nextPutAll: monthIssues ].
			
	html preformatted: [ html render: csv ].

	html anchor  
			document: csv
			mimeType: WAMimeType  textPlain 
			fileName: self date month printString, '-jira.csv';
			with: 'Download CSV'.
]

{ #category : #rendering }
JiraStatsComponent >> selectIssues [
	| monthSprints monthIssues |
	
	monthSprints := self session board sprintsIn: (self date month start to: self date month end).
	monthIssues := monthSprints
		inject: OrderedCollection new
		into: [ :result :sprint | 
			result addAll: sprint costedIssues.
			result ].
		
	^ monthIssues
]
