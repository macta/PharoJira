"
I represent  the root workflow for the Reporting Appliction
"
Class {
	#name : #JiraLoginTask,
	#superclass : #WATask,
	#category : #'JiraPM-Seaside'
}

{ #category : #testing }
JiraLoginTask class >> canBeRoot [
	^true
]

{ #category : #testing }
JiraLoginTask class >> initialize [
	| application |
	
	application := WAAdmin register: self asApplicationAt: 'JiraReporter'.
	application preferenceAt: #sessionClass put: JiraSeasideSession.
]

{ #category : #running }
JiraLoginTask >> go [
	| userDetails |
	
 	userDetails := self call: JiraLoginComponent new.
 
 	userDetails ifNotNil: [
  		self session login: userDetails.
  		self call: JiraSprintReporterComponent new].
]
