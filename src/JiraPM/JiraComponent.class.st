Class {
	#name : #JiraComponent,
	#superclass : #WAComponent,
	#category : #'JiraPM-Seaside'
}

{ #category : #rendering }
JiraComponent >> renderButton: aString sending: aSelector on: html [

	html form: [ 
		html submitButton
			callback: [ aSelector value: self ];
			text: aString ]
	
]

{ #category : #updating }
JiraComponent >> updateRoot: anHtmlRoot [
    super updateRoot: anHtmlRoot.
    anHtmlRoot title: 'Jira Reporter'
]
