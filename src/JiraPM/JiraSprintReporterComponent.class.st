Class {
	#name : #JiraSprintReporterComponent,
	#superclass : #JiraComponent,
	#instVars : [
		'date'
	],
	#category : #'JiraPM-Seaside'
}

{ #category : #accessing }
JiraSprintReporterComponent >> date [
	^ date
]

{ #category : #accessing }
JiraSprintReporterComponent >> date: anObject [
	date := anObject
]

{ #category : #rendering }
JiraSprintReporterComponent >> logout [
	self answer: false
]

{ #category : #rendering }
JiraSprintReporterComponent >> renderContentOn: html [
	html heading: 'Jira Reporter'.
	html heading
		level: 3;
		with: 'View sprint statistics'.
		
	html form: [ 
		html submitButton
		callback: [ self viewSprints ];
		text: 'View Sprints' ].
	
	html break.
	
	html form: [ 
		html dateInput
			options: #(year month);
        callback: [ :value | self date: value ];
        with: Date today.
		html space.
		html submitButton 
			callback: [ self viewStats ];
			text: 'View Stats' ].
		
	html break.
	
	self renderButton: 'Logout' sending: #logout on: html.
	
]

{ #category : #rendering }
JiraSprintReporterComponent >> viewSprints [
	^ self call: JiraSprintComponent new
]

{ #category : #rendering }
JiraSprintReporterComponent >> viewStats [
	^ self
		call:
			(JiraStatsComponent new
				date: self date;
				yourself)
]
