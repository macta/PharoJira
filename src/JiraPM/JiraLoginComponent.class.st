Class {
	#name : #JiraLoginComponent,
	#superclass : #JiraComponent,
	#instVars : [
		'jiraSettings'
	],
	#category : #'JiraPM-Seaside'
}

{ #category : #rendering }
JiraLoginComponent >> initialize [
	super initialize.
	jiraSettings := JiraSettings new.
]

{ #category : #rendering }
JiraLoginComponent >> renderContentOn: html [
	
	html div
		class: 'generic';
		with: [ html
				heading: 'Jira Reporter Login';
				"render: self messageComponent;"
					text: 'Please login with jira server, e-mail and password:';
				form: [ self
						renderTextURLInputOn: html;
						renderTextUsernameInputOn: html;
						renderPasswordInputOn: html;
						renderLoginButtonOn: html ] ]
]

{ #category : #rendering }
JiraLoginComponent >> renderLoginButtonOn: html [
	html submitButton
		callback: [ self validateLogin ];
		text: 'Login'
]

{ #category : #rendering }
JiraLoginComponent >> renderPasswordInputOn: html [
	html passwordInput
		callback: [ :value | jiraSettings password: value ];
		placeholder:'Password, or generated api token';
		value: ''
]

{ #category : #rendering }
JiraLoginComponent >> renderTextURLInputOn: html [
	html textInput
		callback: [ :value | jiraSettings jiraUrl: value ];
		value: 'company.atlassian.net'.
	html space
]

{ #category : #rendering }
JiraLoginComponent >> renderTextUsernameInputOn: html [
	html textInput
		callback: [ :value | jiraSettings username: value ];
		placeholder: 'uername email';
		value: ''.
	html space
]

{ #category : #rendering }
JiraLoginComponent >> validateLogin [
	| jira |
	jira := CachedJira fromSettings: jiraSettings.
	(jira notNil)
		ifTrue: [ self answer: jira ]
		ifFalse: [ self answer: nil ]
]
