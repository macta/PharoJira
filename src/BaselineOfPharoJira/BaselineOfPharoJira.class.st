Class {
	#name : #BaselineOfPharoJira,
	#superclass : #BaselineOf,
	#category : #BaselineOfPharoJira
}

{ #category : #baselines }
BaselineOfPharoJira >> baseline: spec [
	<baseline>
	
	spec
		for: #common
		do: [ self setUpDependencies: spec.
			spec
				package: 'JiraWrappers';
				package: 'JiraPM'with: [ spec requires: #('NeoCSV' 'Seaside3' 'JiraTalk') ];
				group: 'default'with:
					#('JiraPM' 'JiraWrappers') ]
]

{ #category : #baselines }
BaselineOfPharoJira >> setUpDependencies: spec [
	
	spec 
		configuration: 'JiraTalk' 
			with: [ 
				spec
					className: #ConfigurationOfJiraTalk;
					versionString: #'bleedingEdge';
					repository: 'http://smalltalkhub.com/mc/philippeback/JiraTalk/main' ]. 
	spec
		baseline: 'Seaside3' with: [ spec
				repository: 'github://SeasideSt/Seaside:master/repository';
				loads: #('Seaside-Environment' 'JQuery' 'Zinc') ].
			
	spec 
	   baseline: 'NeoCSV' with: [ spec repository: 'github://svenvc/NeoCSV/repository' ].
]
